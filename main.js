let gameState = {
	welcome: true,
	play: false,
	restart: false
}

let playerL = {
	x: 40,
	y: 240,
	w: 20,
	h: 100,
	movingUp: false,
	movingDown: false,
	speed: 8,
	score: 0,
	won: false
}

let playerR = {
	x: 600,
	y: 240,
	w: 20,
	h: 100,
	movingUp: false,
	movingDown: false,
	speed: 8,
	score: 0,
	won: false
}

let ball = {
	x: 320,
	y: 240,
	size: 20,
	speed: {
		x: 5,
		y: -2 + Math.random() * 4
	},
	check: true
}

let font

function preload() {
	font = loadFont( 'Silkscreen-Regular.ttf' )
}

function setup() {
	createCanvas( 640, 480 )
	rectMode( CENTER )
	textFont( font )
	textAlign( CENTER, CENTER )
}

function draw() {
	background( 0 )
	
	fill( 255 )
	noStroke()
	
	if ( gameState.welcome ) {
		textSize( 200 )
		text( 'PONG', 320, 60 )
		
		textSize( 32 )
		text( 'player one', 180, 240 )
		text( 'player two', 460, 240 )
		
		textSize( 96 )
		text( 'S·X', 180, 300 )
		text( 'K·M', 460, 300 )
		
		textSize( 32 )
		fill( 255 * ( floor( frameCount * 0.025 ) % 2 ) )
		text( 'press space to start', 320, 400 )
	}
	
	if ( gameState.play ) {
		rect( playerL.x, playerL.y, playerL.w, playerL.h )      
		rect( playerR.x, playerR.y, playerR.w, playerR.h )
		
		rect( ball.x, ball.y, ball.size )
		
		for ( let i = 0; i < 13; i++) {
			rect( 320, 0 + 2 * i * ball.size, ball.size )
		}
		
		textSize( 96 )
		text( playerL.score, 220, 30 )
		text( playerR.score, 420, 30 )
		
		ball.x += ball.speed.x
		ball.y += ball.speed.y
		
		if ( ball.x > 300 && ball.x < 340 ) {
			ball.check = true
		}
		
		let playerLBallOffset = playerL.w * 0.5 + ball.size * 0.5
		let playerRBallOffset = playerR.w * 0.5 + ball.size * 0.5
		
		if ( ball.x < playerL.x + playerLBallOffset && ball.check ) {
			
			let dist = abs( playerL.y - ball.y )
			let maxDist = playerL.h * 0.5 + ball.size * 0.5
			
			if ( dist < maxDist ) {
				ball.speed.x *= -1
				ball.speed.y = 5 * dist / maxDist * Math.sign( ball.speed.y )
			}
			
			ball.check = false
		}
		
		if ( ball.x > playerR.x - playerRBallOffset && ball.check ) {
			
			let dist = abs( playerR.y - ball.y )
			let maxDist = playerR.h * 0.5 + ball.size * 0.5
			
			if ( dist < maxDist ) {
				ball.speed.x *= -1
				ball.speed.y = 5 * dist / maxDist * Math.sign( ball.speed.y )
			}
			
			ball.check = false
		}
		
		if ( ball.y < 0 + ball.size * 0.5 ) {
			ball.speed.y *= -1
		}
		
		if ( ball.y > 480 - ball.size * 0.5 ) {
			ball.speed.y *= -1
		}
		
		if ( ball.x > 640 + ball.size * 0.5 ) {
			ball.x = 320
			ball.y = 240
			ball.speed.x *= -1
			ball.speed.y = -2 + Math.random() * 4
			playerL.score++
			
			if ( playerL.score === 10 ) {
				gameState.play = false
				gameState.restart = true
				playerL.won = true
			}
		}
		
		if ( ball.x < 0 - ball.size * 0.5 ) {
			ball.x = 320
			ball.y = 240
			ball.speed.x *= -1
			ball.speed.y = -2 + Math.random() * 4
			playerR.score++
			
			if ( playerR.score === 10 ) {
				gameState.play = false
				gameState.restart = true
				playerR.won = true
			}
		}     
		
		if ( playerL.y > 0 + playerL.h * 0.5 ) {
			if ( playerL.movingUp ) playerL.y -= playerL.speed
		}
		if ( playerL.y < 480 - playerL.h * 0.5 ) {
			if ( playerL.movingDown ) playerL.y += playerL.speed
		}
		if ( playerR.y > 0 + playerR.h * 0.5 ) {
			if ( playerR.movingUp ) playerR.y -= playerR.speed
		}
		if ( playerR.y < 480 - playerR.h * 0.5 ) {
			if ( playerR.movingDown ) playerR.y += playerR.speed
		}
	}
	
	if ( gameState.restart ) {
		textSize( 50 )
		if ( playerL.won ) {
			text( 'player one won', 320, 100 )
		}
		if ( playerR.won ) {
			text( 'player two won', 320, 100 )
		}
		
		textSize( 32 )
		text( 'press R to restart', 320, 300 )
		text( 'press ESC to return\nto opening screen', 320, 400 )
	}
}

function keyPressed() {
	if ( gameState.welcome ) {
		if ( keyCode === 32 ) {
			gameState.welcome = false
			gameState.play = true
		}
	}
	
	if ( gameState.play ) {
		if ( keyCode === 83 ) playerL.movingUp = true
		if ( keyCode === 88 ) playerL.movingDown = true
		if ( keyCode === 75 ) playerR.movingUp = true
		if ( keyCode === 77 ) playerR.movingDown = true
	}
	
	if ( gameState.restart ) {
		if ( keyCode === 82 ) {
			gameState.restart = false
			gameState.play = true
			playerL.won = false
			playerR.won = false
			playerL.score = 0
			playerR.score = 0
			playerL.y = 240
			playerR.y = 240
			playerL.movingUp = false
			playerL.movingDown = false
			playerR.movingUp = false
			playerR.movingDown = false
		}
		
		if ( keyCode === 27 ) {
			gameState.restart = false
			gameState.welcome = true
			playerL.won = false
			playerR.won = false
			playerL.score = 0
			playerR.score = 0
			playerL.y = 240
			playerR.y = 240
			playerL.movingUp = false
			playerL.movingDown = false
			playerR.movingUp = false
			playerR.movingDown = false
		}
	}
}

function keyReleased() {
	if ( gameState.play ) {
		if ( keyCode === 83 ) playerL.movingUp = false
		if ( keyCode === 88 ) playerL.movingDown = false
		if ( keyCode === 75 ) playerR.movingUp = false
		if ( keyCode === 77 ) playerR.movingDown = false
	}
}

